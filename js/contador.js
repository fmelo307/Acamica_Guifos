if (typeof Storage !== 'undefined') {
    if (localStorage.visitcount) {
        localStorage.visitcount = Number(localStorage.visitcount) + 1;
        document.getElementById('visitJS').innerHTML = localStorage.visitcount;
    } else {
        localStorage.visitcount = 0;
        document.getElementById('visitJS').innerHTML = localStorage.visitcount;
    }
} else {
    document.getElementById('visitJS').innerHTML =
        "Error! Tu navegador no permite almacenar tus visitas.";
}