const API_KEY = "MOE8e6uGerdha1qiDoKZUG8d26VfSZc8"; // No se de quien es la clave, me rechazaron la solicitud
const TENOR_KEY = "LIVDSRZULELA"; // Clave de prueba de Tenor

/* SUGERENCIAS */
function getSuggestions(number, randMin, randMax) {
    fetch('https://api.giphy.com/v1/gifs/categories?api_key='+API_KEY)
        .then((response) => {
            return response.json();
        })
        .then(data => {
            let rand;
            let min = 0;
            let max = 7;
            for (let i = 0; i < number; i++) {
                if(i!==0){
                    min = max+1;
                }
                if(randMin) {
                    min = randMin;
                }
                max = 7*(i+1);
                if(randMax) {
                    max = randMax;
                }
                rand = Math.floor(Math.random() * (+max - +min)) + +min; // Para que no se repitan los gifs
                if(rand > 27) {
                    rand = 27;
                }

                // Ventanitas con gifs
                let snippet = `
					<div class="window gif_container">
						<div class="title">
							<h3>#${data.data[rand]["name"]}</h3>
							<div class="button close"><a href="#close" onclick="switchSuggestion(this, ${min}, ${max})"><img src="images/close.svg" alt="Cerrar"></a></div>
						</div>
						<div class="gif_outter" data-type="search" data-term="${data.data[rand]["name"]}" onclick="mouseClick(this)">
							<div class="gif_clip" style="background-image: url(${data.data[rand]["gif"]["images"]["fixed_height"]["url"]});"></div>
						</div>
						<a href="#${data.data[rand]["name"]}" class="btn primary" data-type="search" data-term="${data.data[rand]["name"]}" onclick="mouseClick(this)"><span class="border">Ver más...</span></a>
					</div>
				`;
                document.getElementById("suggestions").getElementsByClassName("gif_wrapper")[0].innerHTML += snippet;
            }
        })
        .catch((e) => {
            return e;
        })
}

/*BUSQUEDAS EN LOCAL STORAGE*/
function getSearches() {
    let savedSearches
    if(localStorage.getItem('savedSearches')!= null) {
        savedSearches = localStorage.getItem('savedSearches').split(",");
    } else {
        savedSearches = [];
    }
    return savedSearches;
}
function saveSearches(term) {
    let savedSearches = getSearches();
    let sectionWidth = document.getElementById("prev_searches").offsetWidth;
    let wrapperWidth = document.getElementById("searches_wrapper").offsetWidth;
    if((wrapperWidth+200) >= sectionWidth) {
        savedSearches.pop();
    }
    savedSearches.unshift(term);
    localStorage.setItem('savedSearches', savedSearches);
    showSearches();
}

/*TRENDINGS Y BUSQUEDAS*/
function getResults(type, quantity, term) {
    let url;
    if(type === "search") {
        url = 'https://api.giphy.com/v1/gifs/search?api_key='+API_KEY+'&limit='+quantity+'&q='+term;
    } else if(type === "trends") {
        url = 'https://api.giphy.com/v1/gifs/trending?api_key='+API_KEY+'&limit='+quantity;
    }
    fetch(url)
        .then((response) => {
            return response.json();
        })
        .then(data => {
            let trends = data.data;
            let snippet = '';
            trends.forEach(function(item){
                let title = item.title.split("GIF");
                title = title[0].trim();
                let tags = title.replace(/ /g, " #");
                // Cajitas de gifs
                // No logre crearlo en cajitas de 2col, asi que son todas de 1col
                snippet += `
						<div data-type="link" data-url="${item.url}" class="window nowindow gif_container" onclick="mouseClick(this)">
			                <div class="gif_outter">
			                    <div class="gif_clip" style="background-image: url(${item.images.fixed_height.url});">
			                        <div class="title">
			                            <h3>#${tags}</h3>
			                        </div>
			                    </div>
			                </div>
			            </div>
					`;
            });
            document.getElementById("results").getElementsByClassName("gif_wrapper")[0].innerHTML = snippet;
            if(type === "search") {
                document.getElementById("search_suggestions").style.display = "none";
                document.getElementById("suggestions").style.display = "none";
                document.getElementById("search").value = term;
                document.getElementById("results").getElementsByTagName("h2")[0].innerHTML = term + " (resultados)";
            }
        })
        .catch((e) => {
            return e;
        })
}

/*CIERRE DE CAJITAS Y CAMBIO DE VENTANA*/
function mouseClick(element) {
    let action = element.getAttribute("data-type");
    if(action === "link") {
        let destination = element.getAttribute("data-url");
        window.open(destination);
    } else if(action === "search") {
        let suggestion = element.getAttribute("data-suggestion");
        let queryTerm = element.getAttribute("data-term");
        if(suggestion) {
            saveSearches(queryTerm);
        }
        getResults("search", 20, queryTerm);
    }
}
function switchSuggestion(element, min, max) {
    element = element.parentNode.parentNode.parentNode;
    element.remove();
    getSuggestions(1, min, max);
}

/*BUSCADOR*/
let buscador = document.getElementById("search");
let btnBuscador = document.getElementById("searchFire");
let searchSuggestions = document.getElementById("search_suggestions");
buscador.addEventListener("input", function() {
    let ter = this.value;
    if(ter !== '') {
        btnBuscador.removeAttribute("disabled");
        tenorSearch(ter, 3); // Trae los terminos de tenor
        setTimeout(function(){
            searchSuggestions.style.display = 'block';
        }, 500);
    } else {
        resetSearch();
    }
});
function resetSearch() {
    btnBuscador.setAttribute("disabled", "disabled");
    setTimeout(function(){
        searchSuggestions.style.display = 'none';
    }, 500);
    getResults("trends", 20, '');
    document.getElementById("results").getElementsByTagName("h2")[0].innerHTML = "Tendencias:";
    document.getElementById("suggestions").style.display = "block";
}
btnBuscador.addEventListener("click", function(){
    getResults("search", 20, buscador.value);
    saveSearches(buscador.value);
});

/*TERMINOS SUGERIDOS*/
function tenorSearch(searchTerm, quantity) {
    fetch('https://api.tenor.com/v1/search_suggestions?q='+searchTerm+'&key='+TENOR_KEY)
        .then((response) => {
            return response.json();
        })
        .then(data => {
            let terms = data.results;
            let snippet = '';
            for (let i = 0; i < quantity; i++) {
                snippet += `
					<a class="suggestion btn" href="#${terms[i]}" data-type="search" data-term="${terms[i]}" data-suggestion="true" onclick="mouseClick(this)">
						<span class="border">${terms[i]}</span>
					</a>
				`;
            }
            document.getElementById("search_suggestions").innerHTML = snippet;
        })
        .catch((e) => {
            return e;
        })
}

function showSearches() {
    if(getSearches()!= null) {
        let savedSearches = getSearches();
        let snippet = '';
        savedSearches.forEach(function(item){
            snippet += `
				<a href="#${item}" class="btn primary" data-type="search" data-term="${item}" onclick="mouseClick(this)">
					<span class="border">#${item}</span>
				</a>
			`;
        });
        document.getElementById("searches_wrapper").innerHTML = snippet;
    }
}

showSearches();
getSuggestions(4);
getResults("trends", 12, '');