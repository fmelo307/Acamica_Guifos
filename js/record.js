const video = document.getElementById("user_video");
const previo = document.getElementById("gif_prev");
const title = document.getElementById("capture_title");
let recorder;
let blob;
let userGIF;

/*FUNCION COPIADA/IMPORTADA DEL WEB-RTC*/
function getVideoAndRecord() {
    navigator.mediaDevices.getUserMedia({
        audio: false,
        video: {
            aspectRatio: 1.7777777778,
            height: { max: 434 }
        }
    })
        .then(function(stream) {
            video.srcObject = stream;
            video.play();

            document.getElementById("capture").onclick = function(event) {
                event.preventDefault();
                document.getElementById("capture").style.visibility = "hidden";

                setTimeout(function() {
                    recorder = RecordRTC(stream, {
                        type: 'gif',
                        frameRate: 1,
                        quality: 10,
                        width: video.videoWidth,
                        height: video.videoHeight,
                        timeSlice: 1000
                    });
                    recorder.startRecording();
                    recorder.camera = stream;
                    document.getElementById("capture").style.display = "none";
                    document.getElementById("stop").style.display = "inline-block";
                    (function looper() {
                        if(!recorder) {
                            return;
                        }
                    })();
                },0);
            }

            // Manejo del DOM
            document.getElementById("stop").onclick = function(event) {
                event.preventDefault();
                title.innerHTML = "Vista prvia";
                document.getElementById("stop").style.display = "none";
                document.getElementById("repeat").style.display = "inline-block";
                document.getElementById("upload").style.display = "inline-block";
                recorder.stopRecording(function() {
                    blob = recorder.getBlob();
                    userGIF = URL.createObjectURL(blob);
                    let vieoOuttherHeight = video.offsetHeight;
                    recorder.camera.stop();
                    recorder = null;
                    video.src = video.srcObject = null;
                    video.style.display = "none";
                    previo.src = userGIF;
                    previo.height = vieoOuttherHeight;
                    previo.style.display = "block";
                });
            }
        })
        .catch(function(error) {
            return error;
        })
}

/*GIFO EN LOCAL STORAGE*/
function getGuifos() {
    let userGuifos
    if(localStorage.getItem('userGuifos')!= null) {
        userGuifos = localStorage.getItem('userGuifos').split(",");
    } else {
        userGuifos = [];
    }
    return userGuifos;
}

/*CAPTURAR*/
document.getElementById("create").onclick = function(event) {
    event.preventDefault();
    getVideoAndRecord();
    document.getElementById("check").style.display = "none";
    results.style.display = "none";
    document.getElementById("creation").style.display = "block";
}

/*REPETIR*/
document.getElementById("repeat").onclick = function(event) {
    event.preventDefault();
    title.innerHTML = "Un chequeo antes de empezar";
    document.getElementById("repeat").style.display = "none";
    document.getElementById("upload").style.display = "none";
    previo.style.display = "none";
    video.style.display = "block";
    document.getElementById("capture").style.display = "inline-block";
    document.getElementById("capture").style.visibility = "visible";
    getVideoAndRecord();
}

/*SUBIR*/
document.getElementById("upload").onclick = function(event) {
    event.preventDefault();
    title.innerHTML = "Subiendo Guifo";
    document.getElementById("repeat").style.display = "none";
    document.getElementById("upload").style.display = "none";
    document.getElementById("uploading").style.display = "block";
    document.getElementById("cancel").style.display = "inline-block";
    previo.style.display = "none";
    let form = new FormData();
    form.append('file', blob, 'macGif.gif');
    fetch('https://upload.giphy.com/v1/gifs?api_key='+API_KEY, {
        method: 'POST',
        body: form,
        redirect: 'follow'
    })
        .then((response) => response.json())
        .then((result) => {
            let uploadedId = result.data.id;
            let userGuifos = getGuifos();
            userGuifos.unshift(uploadedId);
            localStorage.setItem('userGuifos', userGuifos);
            return fetch('https://api.giphy.com/v1/gifs/'+uploadedId+'?api_key='+API_KEY); // Sube a Giphy
        })
        .then((response) => {
            return response.json();
        })
        .then(data => {
            document.getElementById("gifURL").value = data.data.url;
            document.getElementById("gifResult").src = userGIF;
            results.style.display = "block";
            document.getElementById("creation").style.display = "none";
            document.getElementById("success").style.display = "block";
            savedGuifos();
        })
        .catch((e) => {
            console.error('Error:', e);
        });
}

/*COPIAR LINK*/
document.getElementById("copy").onclick = function(event) {
    event.preventDefault();
    document.getElementById("gifURL").select();
    document.execCommand("copy");
    alert("Link de Giphy: "+document.getElementById("gifURL").value);
}

/*BAJAR GIF*/
document.getElementById("downloadGuifo").onclick = function(event) {
    event.preventDefault();
    invokeSaveAsDialog(blob);
}