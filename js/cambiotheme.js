document.getElementById("day_theme").addEventListener('click', function() {
    set_theme('css/light_theme.css');
}, false);

document.getElementById("dark_theme").addEventListener('click', function() {
    set_theme('css/dark_theme.css');
}, false);