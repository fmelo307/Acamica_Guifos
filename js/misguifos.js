const API_KEY = "MOE8e6uGerdha1qiDoKZUG8d26VfSZc8"; // No se de quien es la clave, me rechazaron la solicitud
const results = document.getElementById("results");

function savedGuifos() {
    if(localStorage.getItem('userGuifos')!= null) {
        var snippet = '';
        var userGuifos = localStorage.getItem('userGuifos').split(",");
        userGuifos.forEach(function(item){
            fetch('https://api.giphy.com/v1/gifs/'+item+'?api_key='+API_KEY)
                .then((response) => {
                    return response.json();
                })
                .then(data => {
                    // Cajita de gifs
                    snippet += `
							<div class="window nowindow gif_container fixer" onclick="window.open('${data.data.url}')">
				                <div class="gif_outter">
				                    <div class="gif_clip" style="background-image: url(${data.data.images.fixed_height.url});">
				                    </div>
				                </div>
				            </div>
                    `;
                    document.getElementById("results").getElementsByClassName("gif_wrapper")[0].innerHTML = snippet;
                })
                .catch((e) => {
                    return e;
                });
        });
    }
}

savedGuifos();