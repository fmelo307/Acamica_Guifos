function check_theme() {
    let currentTheme = localStorage.getItem('theme');
    if(!currentTheme) {
        set_theme('css/light_theme.css');
    } else {
        set_theme(currentTheme);
    }
}
function set_theme(stylesheet) {
    document.getElementById("theme").setAttribute("href", stylesheet);
    localStorage.setItem('theme', stylesheet);
}

check_theme();